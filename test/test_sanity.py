import allure
import pytest

from page_objects.main_page import MainPage

@allure.suite("Sanity Tests")
class TestSanity:
    @pytest.mark.sanity
    @pytest.mark.main_page_sanity
    @allure.title("Main Page Verification")
    def test_main_page(self, main_page: MainPage):
        assert main_page.python_logo.is_displayed(), "Python logo is not shown"

    @pytest.mark.sanity
    @pytest.mark.about_page_sanity
    @allure.title("About page verification")
    def test_about_page(self, main_page: MainPage):
        about_page = main_page.go_to_about_menu()

        assert about_page.about_banner.is_displayed(), "About banner is not displayed"


    @pytest.mark.signup
    @allure.title("Sign Up process validation")
    def test_signup(self, main_page: MainPage, email, username, password):
        signup_page = main_page.click_on_membership_button().go_to_signup_page()

        email_confirmation_page = signup_page.enter_email(email).enter_username(username).enter_password(password).confirm_password(password).click_signup_button()

        assert email_confirmation_page.email_confirmation_message.is_displayed(), "Email Confirmation message is not shown"

        expected_confirmation_message = f"Confirmation e-mail sent to {email}."

        assert email_confirmation_page.email_confirmation_message.text == expected_confirmation_message, f"Confirmation message is incorrect:\n" \
                                                                                                         f"{email_confirmation_page.email_confirmation_message.text}\n" \
                                                                                                         f"Sould be:\n" \
                                                                                                         f"{expected_confirmation_message}"