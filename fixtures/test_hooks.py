def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="google-chrome", help="Browsers to run test on. Available browsers: google-chrome, firefox, edge")
