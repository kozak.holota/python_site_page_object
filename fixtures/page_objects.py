import pytest

from helper.fixture_helpers import get_browser
from selenium.webdriver import Remote

from page_objects.main_page import MainPage


@pytest.fixture
def main_page(request):
    web_driver: Remote = get_browser(request.config.getoption("--browser"))
    web_driver.get("https://www.python.org/")

    yield MainPage(web_driver)

    web_driver.quit()
