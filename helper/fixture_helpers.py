from selenium.webdriver import Chrome, Firefox, Edge, DesiredCapabilities

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager


def get_browser(browser_name: str):
    if browser_name == "google-chrome":
        chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
        chrome_browser.maximize_window()

        return chrome_browser

    elif browser_name == "firefox":
        capabilities = DesiredCapabilities.FIREFOX
        firefox_browser = Firefox(executable_path=GeckoDriverManager().install(), desired_capabilities=capabilities)
        firefox_browser.maximize_window()

        return firefox_browser

    elif browser_name == "edge":
        edge_browser = Edge(executable_path=EdgeChromiumDriverManager().install())
        edge_browser.maximize_window()

        return edge_browser

    else:
        raise ValueError(f"Incorrect browser '{browser_name}. Available browsers: google-chrome, firefox, edge'")
