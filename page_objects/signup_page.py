import allure
from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC

from page_objects.email_confirmation_page import EmailConfirmationPage


class SignupPage(BasePage):
    @property
    def email_field(self):
        return self._wait.until(EC.element_to_be_clickable((By.ID, "id_email")))

    @property
    def username_field(self):
        return self._wait.until(EC.element_to_be_clickable((By.ID, "id_username")))

    @property
    def password_field(self):
        return self._wait.until(EC.element_to_be_clickable((By.ID, "id_password1")))

    @property
    def password_confirmation_field(self):
        return self._wait.until(EC.element_to_be_clickable((By.ID, "id_password2")))

    @property
    def signup_button(self):
        return self._wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Sign Up')]")))

    @allure.step("Enter email: {1}")
    def enter_email(self, email):
        self.email_field.send_keys(email)

        return self

    @allure.step("Enter username: {1}")
    def enter_username(self, username):
        self.username_field.send_keys(username)
        return self

    @allure.step("Enter password: {1}")
    def enter_password(self, password):
        self.password_field.send_keys(password)
        return self

    @allure.step("Confirm password: {1}")
    def confirm_password(self, password):
        self.password_confirmation_field.send_keys(password)

        return self

    def click_signup_button(self):
        self.signup_button.click()

        self.get_page_screenshot()

        return EmailConfirmationPage(self._web_driver)