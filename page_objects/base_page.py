import allure
from selenium.webdriver import Remote, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, web_driver: Remote):
        self._web_driver = web_driver
        self._wait = WebDriverWait(self._web_driver, 20)
        self._action = ActionChains(self._web_driver)

    @property
    def python_logo(self):
        return self._wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "python-logo")))

    @allure.step("Navigating to menu '{1}")
    def get_menu(self, text: str):
        href_text = text.lower() if len(text.split(" ")) == 1 else "-".join(text.lower().split(" "))
        return self._wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, f"a[href='/{href_text}/']")))

    def go_to_about_menu(self):
        from page_objects.about_page import AboutPage
        self.get_menu("About").click()

        return AboutPage(self._web_driver)

    def get_page_screenshot(self):
        allure.attach(self._web_driver.get_screenshot_as_png(), attachment_type=allure.attachment_type.PNG)