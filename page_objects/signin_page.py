import allure
from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC

from page_objects.signup_page import SignupPage


class SigninPage(BasePage):
    @property
    def signup_button(self):
        return self._wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '[href="/accounts/signup/"]')))

    @allure.step("Navigate to Sign Up page")
    def go_to_signup_page(self):
        self.signup_button.click()

        return SignupPage(self._web_driver)
