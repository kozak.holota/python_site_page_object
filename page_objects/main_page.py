import allure
from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC

from page_objects.signin_page import SigninPage


class MainPage(BasePage):
    @property
    def became_member_button(self):
        return self._wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '[href="/users/membership/"]')))

    @allure.step("Click on the button 'Became a member'")
    def click_on_membership_button(self):
        self.became_member_button.click()

        return SigninPage(self._web_driver)
