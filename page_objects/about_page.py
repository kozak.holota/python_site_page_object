from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC


class AboutPage(BasePage):
    @property
    def about_banner(self):
        return self._wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "about-banner")))