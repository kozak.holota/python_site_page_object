from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC

class EmailConfirmationPage(BasePage):
    @property
    def email_confirmation_message(self):
        return self._wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "p[role=general]")))